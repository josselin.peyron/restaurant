package com.restaurant

import com.restaurant.data.AffectationRequest
import com.restaurant.data.Employe
import io.micronaut.http.HttpRequest
import io.micronaut.http.client.RxHttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.client.exceptions.HttpClientResponseException
import io.micronaut.runtime.server.EmbeddedServer
import io.micronaut.test.annotation.MicronautTest
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import javax.inject.Inject

@MicronautTest
class AffectationControllerSpec {

    @Inject
    @field:Client("/")
    lateinit var client: RxHttpClient

    @Inject
    lateinit var server: EmbeddedServer

    @Test
    fun `Exemple 1`() {
        val employes = ArrayList<Employe>()
        employes.add(Employe("Bob", listOf("B", "W")))
        employes.add(Employe("Bill", listOf("B", "W", "P", "C")))
        employes.add(Employe("Jack", listOf("B")))
        val jobs = mapOf("B" to 1, "W" to 1, "P" to 0, "C" to 1)

        val expectedResult = """{"affectations":{"Bob":"W","Bill":"C","Jack":"B"}}"""

        testAffectation(employes, jobs, expectedResult)
    }

    @Test
    fun `Exemple 2`() {
        val employes = ArrayList<Employe>()
        employes.add(Employe("Bob", listOf("B", "P", "C")))
        employes.add(Employe("Bill", listOf("B")))
        val jobs = mapOf("B" to 1, "W" to 1, "P" to 0, "C" to 1)

        val expectedResult = """{"error":"Not enough Waiter available"}"""

        testAffectationError(employes, jobs, expectedResult)
    }

    @Test
    fun `Exemple 3`() {
        val employes = ArrayList<Employe>()
        employes.add(Employe("Bob", listOf("B", "P", "C")))
        employes.add(Employe("Bill", listOf("B")))
        val jobs = mapOf("B" to 1, "W" to 1, "P" to 0, "C" to 1)

        val expectedResult = """{"error":"Too much jobs for employes"}"""

        testAffectationError(employes, jobs, expectedResult)
    }


    @Test
    fun `Exemple 4`() {
        val employes = ArrayList<Employe>()
        employes.add(Employe("Bob", listOf("B", "P","C")))
        employes.add(Employe("Bill", listOf("B", "W",)))
        employes.add(Employe("Jack", listOf("B")))
        val jobs = mapOf("B" to 1, "W" to 1, "P" to 0, "C" to 1)

        val expectedResult = """{"affectations":{"Bob":"C","Bill":"W","Jack":"B"}}"""

        testAffectation(employes, jobs, expectedResult)
    }


    @Test
    fun `Exemple 5`() {
        val employes = ArrayList<Employe>()
        employes.add(Employe("Bob", listOf("B", "P","C")))
        employes.add(Employe("Bill", listOf("B", "C",)))
        employes.add(Employe("Jack", listOf("B", "W")))
        val jobs = mapOf("B" to 0,"W" to 1, "P" to 0, "C" to 2)

        val expectedResult = """{"affectations":{"Bob":"C","Bill":"C","Jack":"W"}}"""

        testAffectation(employes, jobs, expectedResult)
    }

    private fun testAffectation(employes: java.util.ArrayList<Employe>, jobs: Map<String, Int>, expectedResult: String) {
        val affectationRequest = AffectationRequest(employes, jobs)
        val request: HttpRequest<Any> = HttpRequest.POST("/affectation", affectationRequest)
        val body = client.toBlocking().retrieve(request)

        assertNotNull(body)

        assertEquals(expectedResult, body)
    }

    private fun testAffectationError(employes: java.util.ArrayList<Employe>, jobs: Map<String, Int>, expectedResult: String) {
        val affectationRequest = AffectationRequest(employes, jobs)
        val request: HttpRequest<Any> = HttpRequest.POST("/affectation", affectationRequest)


        assertThrows<HttpClientResponseException>(expectedResult, { client.toBlocking().retrieve(request) })

    }

}
