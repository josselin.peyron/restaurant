package com.restaurant.data

data class Affectation(val idEmployee: String, val jobType: String)
