package com.restaurant.data

data class Job(val type: String, val number: Int)
