package com.restaurant.data

data class AffectationRequest(val employes: List<Employe>, val jobs: Map<String, Int>)
