package com.restaurant

import com.restaurant.error.NotEnoughWaiterException
import com.restaurant.error.TooMuchJobsException
import io.micronaut.context.annotation.Requires
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Produces
import io.micronaut.http.server.exceptions.ExceptionHandler
import javax.inject.Singleton

data class CustomError(val error: String)

@Produces
@Singleton
@Requires(classes = [NotEnoughWaiterException::class, ExceptionHandler::class])
class NotEnoughWaiterExceptionErrorHandler : ExceptionHandler<NotEnoughWaiterException, HttpResponse<*>> {
    override fun handle(request: HttpRequest<Any>, exception: NotEnoughWaiterException?): HttpResponse<*> {
        return HttpResponse.badRequest(CustomError("""Not enough Waiter available"""))
    }
}

@Produces
@Singleton
@Requires(classes = [TooMuchJobsException::class, ExceptionHandler::class])
class TooMuchJobsExceptionErrorHandler : ExceptionHandler<TooMuchJobsException, HttpResponse<*>> {
    override fun handle(request: HttpRequest<Any>, exception: TooMuchJobsException?): HttpResponse<*> {
        return HttpResponse.badRequest(CustomError("""Too much jobs for employes"""))
    }
}
