package com.restaurant.service

import com.restaurant.data.AffectationResponse
import com.restaurant.data.Employe
import com.restaurant.error.NotEnoughWaiterException
import com.restaurant.error.TooMuchJobsException
import javax.inject.Singleton

@Singleton
open class AffectationService {
    open fun computeAffectation(employes: List<Employe>, jobs: Map<String, Int>): AffectationResponse {
        val results = HashMap<String, String>()
        val sortedEmployes = sortEmployesByNUmberOfComptencesDesc(employes)
        val sortedJobs = sortJobsByValueDesc(jobs)
        val usedEmployes = ArrayList<Employe>()

        val countTotalJobs = sortedJobs.values.sum()

        if (sortedEmployes.size < countTotalJobs) throw TooMuchJobsException()

        for (job in sortedJobs) {
            for (i in 1..job.value) {
                val employe: Employe = sortedEmployes.firstOrNull { !usedEmployes.contains(it) && it.c.contains(job.key) } ?: throw NotEnoughWaiterException()
                results[employe.id] = job.key
                usedEmployes.add(employe)
            }
        }

        return AffectationResponse(results)
    }

    private fun sortEmployesByNUmberOfComptencesDesc(employes: List<Employe>) = employes.sortedWith { e1: Employe, e2: Employe -> e1.c.size - e2.c.size }

    private fun sortJobsByValueDesc(jobs: Map<String, Int>): Map<String, Int> {
        return jobs.toList().sortedBy { (_, value) -> value }.reversed().toMap()
    }
}
