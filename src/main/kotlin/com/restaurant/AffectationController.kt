package com.restaurant

import com.restaurant.data.AffectationRequest
import com.restaurant.data.AffectationResponse
import com.restaurant.service.AffectationService
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Post
import javax.inject.Inject

@Controller("/affectation")
class AffectationController {

    @Inject
    lateinit var affectationService: AffectationService

    @Post(value = "/")
    fun computeAffectation(@Body affectationRequest: AffectationRequest): AffectationResponse {
        return affectationService.computeAffectation(affectationRequest.employes, affectationRequest.jobs)
    }
}
