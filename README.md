### Exemple d'API Rest réalisée en Kotlin avec Micronaut ### 

## Build de l'application et lancement des tests unitaires

Dans le répertoire de l'application, lancer la commande :
- `gradlew build`

## Démarrage de l'application

Dans le répertoire de l'application, lancer la commande :
- `gradlew run`
- l'application devrait être démarrée : http://localhost:8080

## Tester l'application web

- Lancer un requête POST sur l'url http://localhost:8080/affectation 
- avec comme body content type: `"application/json"` 
 - et un body :

`{
  "employes": [
	{"id": "Bob", "c": ["B","W"]},
	{"id": "Bill", "c": ["B","W","P","C"]},
	{"id": "Jack", "c": ["B"]}
  ],
  "jobs": {
	"B": 1, "W": 1, "P": 0, "C": 1
  }
}
`

- exemple en curl : 
`curl -H "Content-Type: application/json" -X POST http://localhost:8080/affectation -d "{  \"employes\": [ {\"id\": \"Bob\", \"c\": [\"B\",\"W\"]}, {\"id\": \"Bill\", \"c\": [\"B\",\"W\",\"P\",\"C\"]}, {\"id\": \"Jack\", \"c\": [\"B\"]}  ],  \"jobs\": { \"B\": 1, \"W\": 1, \"P\": 0, \"C\": 1  }}"`


## Micronaut 2.5.4 Documentation

- [User Guide](https://docs.micronaut.io/2.5.4/guide/index.html)
- [API Reference](https://docs.micronaut.io/2.5.4/api/index.html)
- [Configuration Reference](https://docs.micronaut.io/2.5.4/guide/configurationreference.html)
- [Micronaut Guides](https://guides.micronaut.io/index.html)
---

## Feature http-client documentation

- [Micronaut HTTP Client documentation](https://docs.micronaut.io/latest/guide/index.html#httpClient)

